const sha256 = require('crypto-js/sha256')

class Block {
    constructor (hashAnterior = null, msg = 'Genesis block') {
        this.hashAnterior = hashAnterior
        this.msg = msg
        this.hash = this.geraHash()
        this.decimal = this.hexToDec(this.hash)/Math.pow(10,76)
		this.limite = 10
    }

    geraHash() {
        return sha256( this.hashAnterior + JSON.stringify(this.msg) ).toString()
    }

	hexToDec(hex) {
    var result = 0, digitValue;
    hex = hex.toLowerCase();
    for (var i = 0; i < hex.length; i++) {
        digitValue = '0123456789abcdefgh'.indexOf(hex[i]);
        result = result * 16 + digitValue;
    }
    return result;
}
    
}

module.exports = Block