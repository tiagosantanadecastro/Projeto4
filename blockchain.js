const Block = require('./block')

class Blockchain {
    constructor() {
        this.blocos = [new Block()]
        
    }

    pegaUltimoBloco() {
        return this.blocos[this.blocos.length - 1]
    }

    addBloco(msg) {
      
        const hashAnterior = this.pegaUltimoBloco().hash

        const block = new Block(hashAnterior, msg)


        this.blocos.push(block)
    }
	
	getHash(int o){
		return this.blocos[o].hash;

      isValid() {
        for (let i = 1; i < this.blocos.length; i++) {
            const blocoAtual = this.blocos[i]
            const blocoAnterior = this.blocos[i - 1]

            if (blocoAtual.hashAnterior !== blocoAnterior.hash) {
                return false
            }
			if (blocoAnterior.decimal>blocoAnterior.limite){
				return false
			}
        }
        return true
}
}

module.exports = Blockchain